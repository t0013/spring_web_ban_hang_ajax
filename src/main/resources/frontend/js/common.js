
const BASE_DOMAIN = "http://localhost:8080";

class Email {
    email = "";

    constructor(email) {
        this.email = email;
    }

    validate() {
        let format = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if(!this.email.match(format)) {
            alert("Email không đúng định dạng!");
            $("#email").focus();
            return false;
        }

        return true;
    }
}

class UtilsCommons {

    getIds() {
        let list = document.querySelectorAll('input[type="checkbox"]:checked');
        let ids = [];

        for (let i = 0; i < list.length; i++) {
            ids.push(list[i].value);
        }

        return ids;
    }
}

// // get ids
// const getIds = function() {
//     let list = document.querySelectorAll('input[type="checkbox"]:checked');
//     let ids = [];
//
//     for (let i = 0; i < list.length; i++) {
//         ids.push(list[i].value);
//     }
//
//     return ids;
// }