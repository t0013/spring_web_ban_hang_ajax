package com.tn.controller;

import com.tn.dto.DepartmentListDTO;
import com.tn.entity.Department;
import com.tn.repository.DepartmentRepository;
import com.tn.utilhttpstatus.UtilHttpStatus;
import org.hibernate.exception.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("department")
public class DepartmentController {

    private final ModelMapper modelMapper;

    private final DepartmentRepository departmentRepo;

    public DepartmentController(DepartmentRepository departmentRepo,
                                ModelMapper modelMapper) {
        this.departmentRepo = departmentRepo;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public ResponseEntity<?> getAll() {
        List<Department> departments = departmentRepo.findAll();
        List<DepartmentListDTO> dtos = modelMapper.map(departments, new TypeToken<List<Department>>(){}.getType());

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    private ResponseEntity<?> checkExists(Department department) {
        Optional<Department> tmp = departmentRepo.findByDepartmentName(department.getDepartmentName().trim());
        if (tmp.isPresent()) {
            return new ResponseEntity<>(UtilHttpStatus.Department.EXISTS_DEPARTMENT_NAME.name(), HttpStatus.BAD_REQUEST);
        }

        return null;
    }

    @PostMapping
    public ResponseEntity<?> save(@RequestBody Department department) {
        ResponseEntity<?> res = checkExists(department);
        if (res != null) {
            return res;
        }

        departmentRepo.save(department);

        return new ResponseEntity<>(HttpStatus.OK, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public Department edit(@PathVariable Integer id) {
        Optional<Department> department = departmentRepo.findById(id);
        return department.get();
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody Department department) {
        try {
            departmentRepo.save(department);
        } catch (DataIntegrityViolationException ex) {
            return new ResponseEntity<>(UtilHttpStatus.Department.EXISTS_DEPARTMENT_NAME, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK, HttpStatus.OK);
    }

    @DeleteMapping("{ids}")
    public ResponseEntity<?> deleteIds(@PathVariable String ids) {
        List<Integer> listId = new ArrayList<>();
        String[] items = ids.split(",");

        for (int i = 0; i < items.length; i++) {
            listId.add(Integer.parseInt(items[i]));
        }

        departmentRepo.deleteAllById(listId);
        return new ResponseEntity<>(HttpStatus.OK, HttpStatus.OK);
    }

}
