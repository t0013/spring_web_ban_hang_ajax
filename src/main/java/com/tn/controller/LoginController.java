package com.tn.controller;

import com.tn.dto.AccountLoginDTO;
import com.tn.entity.Account;
import com.tn.repository.AccountRepository;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Optional;

@RestController
public class LoginController {

    private final ModelMapper modelMapper;

    private final AccountRepository accountRepo;

    public LoginController(AccountRepository accountRepo,
                           ModelMapper modelMapper) {
        this.accountRepo = accountRepo;
        this.modelMapper = modelMapper;
    }

    @PostMapping("account/login")
    public ResponseEntity<?> login(Principal principal) {
        String username = principal.getName();
        Optional<Account> opAccount = accountRepo.findByUsername(username);

        AccountLoginDTO accountLoginDTO = modelMapper.map(opAccount.get(), AccountLoginDTO.class);

        // trả về 1 object như new Account() hoặc 1 entity account lấy từ db)
        // vì ajax lấy res dạng json, nếu là String, null, không parse được sang json
        return new ResponseEntity<>(accountLoginDTO, HttpStatus.OK);
    }

}
