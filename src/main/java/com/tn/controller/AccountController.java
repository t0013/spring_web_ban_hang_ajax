package com.tn.controller;

import com.tn.entity.Account;
import com.tn.repository.AccountRepository;
import com.tn.utilhttpstatus.UtilHttpStatus;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("account")
public class AccountController {

    private final AccountRepository accountRepo;

    public AccountController(AccountRepository accountRepo) {
        this.accountRepo = accountRepo;
    }

    @GetMapping
    public ResponseEntity<?> getAll() {
        List<Account> accounts = accountRepo.findAll();

        return new ResponseEntity<>(accounts, HttpStatus.OK);
    }

    private ResponseEntity<?> checkExists(Account account) {
        Optional<Account> tmp = accountRepo.findByUsername(account.getUsername().trim());
        if (tmp.isPresent()) {
            return new ResponseEntity<>(UtilHttpStatus.Account.EXISTS_USERNAME.name(), HttpStatus.BAD_REQUEST);
        }

        tmp = accountRepo.findByUsername(account.getEmail().trim());
        if (tmp.isPresent()) {
            return new ResponseEntity<>(UtilHttpStatus.Account.EXISTS_EMAIL.name(), HttpStatus.BAD_REQUEST);
        }

        return null;
    }

    @PostMapping
    public ResponseEntity<?> save(@RequestBody Account account) {
        ResponseEntity<?> res = checkExists(account);
        if (res != null) {
            return res;
        }

        account.setPassword("$2a$12$vXE10mTbcLwbYZ6JR84Li.iDzIhe7w0RYNOfajrxcD2pGCNpETQAi");
        accountRepo.save(account);

        return new ResponseEntity<>(HttpStatus.OK, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public Account edit(@PathVariable Integer id) {
        Optional<Account> account = accountRepo.findById(id);
        return account.get();
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody Account account) {
        accountRepo.save(account);
        return new ResponseEntity<>(HttpStatus.OK, HttpStatus.OK);
    }

    @DeleteMapping("{ids}")
    public ResponseEntity<?> deleteIds(@PathVariable String ids) {
        List<Integer> listId = new ArrayList<>();
        String[] items = ids.split(",");

        for (int i = 0; i < items.length; i++) {
            listId.add(Integer.parseInt(items[i]));
        }

        accountRepo.deleteAllById(listId);
        return new ResponseEntity<>(HttpStatus.OK, HttpStatus.OK);
    }

}
