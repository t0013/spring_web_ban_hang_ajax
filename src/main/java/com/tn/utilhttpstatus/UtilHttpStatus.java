package com.tn.utilhttpstatus;

public class UtilHttpStatus {

    public enum Department {
        EXISTS_DEPARTMENT_NAME
    }

    public enum Account {
        EXISTS_USERNAME,
        EXISTS_EMAIL
    }

}
