package com.tn.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank(message = "Không được để trống username")
    @Length(max = 100, message = "username không được qua 100 ký tự")
    @Pattern(regexp = "^[a-zA-Z0-9]+([._]?[a-zA-Z0-9]+)*$", message = "Không chứa các ký tự đặc biệt")
    private String username;

    @NotBlank(message = "Không được để trống password")
    @Length(max = 100, message = "password không được qua 100 ký tự")
    private String password;

    @NotBlank(message = "Không được để trống fullName")
    @Length(max = 100, message = "fullName không được qua 100 ký tự")
    private String fullName;

    @NotBlank(message = "Không được để trống email")
    @Length(max = 100, message = "email không được qua 100 ký tự")
    private String email;

    private String role;

    private Boolean enabled;

    @ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;

}
